#!/usr/bin/env python3

import sys, os
import json
from graph_tool.all import Graph
from graph_tool.search import dfs_iterator
from graph_tool.topology import is_DAG, topological_sort
import subprocess
import argparse
from numpy import array, zeros
from tqdm import tqdm # progress tracker
import time

__author__ = "Jasmijn Baaijens"
__license__ = "MIT"

usage = "Build contig variation graph using vg msga and compute node abundances."


def main():
    parser = argparse.ArgumentParser(prog='build_graph_msga.py', description=usage)
    parser.add_argument('-f', '--forward', dest='forward', type=str, required=True, help='Forward reads in fastq format')
    parser.add_argument('-r', '--reverse', dest='reverse', type=str, help='Reverse reads in fastq format')
    parser.add_argument('-c', '--contigs', dest='contigs', type=str, required=True, help='Input contigs in fastq format')
    parser.add_argument('-vg', '--vg_path', dest='vg', type=str, required=True, help="Path to vg executable")
    parser.add_argument('-t', '--threads', default=8, help="Set number of threads used for vg.")
    parser.add_argument('-w', '--msga_w', dest='msga_w', default=128, help="Set alignment band width parameter for vg msga.")
    parser.add_argument('--reuse_graph', dest='reuse_graph', action='store_true')
    args = parser.parse_args()

    global_t1_start = time.perf_counter()
    global_t2_start = time.process_time()

    filepath = os.path.dirname(os.path.abspath(__file__))
    graph_name = "contig_graph"
    gfa_file = "{}.norm.gfa".format(graph_name)
    aln_name = "{}.norm.aln".format(graph_name)
    aln_file = "{}.filtered.json".format(aln_name)
    if not args.reuse_graph:
        t1_start = time.perf_counter()
        t2_start = time.process_time()
        # sort contigs
        sorted_contigs = "sorted_contigs.fasta"
        subprocess.check_call(
            "python {}/sort_contigs.py {} {}".format(filepath, args.contigs, sorted_contigs),
            shell=True
        )
        with open(sorted_contigs, 'r') as f:
            line = f.readline()
            base_seq = line.lstrip('>').rstrip()

        # build initial graph
        print("Building initial graph")
        vg = args.vg
        subprocess.check_call( vg +
            " msga -f {} -b {} -t {} -a -w {} > {}.vg".format(
                sorted_contigs, base_seq, args.threads, args.msga_w, graph_name),
            shell=True
        )

        # normalize graph
        print("Normalizing graph")
        subprocess.check_call( vg +
            " mod -n {0}.vg | {1} mod -X 32 - | {1} ids -c - > {0}.norm.vg".format(
                graph_name, vg),
            shell=True
        )

        # convert vg to gfa
        print("Convert graph to GFA")
        subprocess.check_call( vg +
            " view {}.norm.vg > {}".format(graph_name, gfa_file),
            shell=True
        )

        t1_stop = time.perf_counter()
        t2_stop = time.process_time()
        print("Variation graph is ready")
        print("Elapsed time: {:.1f} seconds".format(t1_stop-t1_start))
        print("CPU process time: {:.1f} seconds".format(t2_stop-t2_start))
        print()

        # index graph
        t1_start = time.perf_counter()
        t2_start = time.process_time()
        print("Building index for graph")
        subprocess.check_call("mkdir -p tmp", shell=True)
        subprocess.check_call( "export TMPDIR=tmp && " + vg +
            " index -x {0}.norm.xg -g {0}.norm.gcsa -k 16 -X 2 -Z 500 -t {1} {0}.norm.vg".format(
                graph_name, args.threads),
            shell=True
        )
        t1_stop = time.perf_counter()
        t2_stop = time.process_time()
        print("Graph index is ready")
        print("Elapsed time: {:.1f} seconds".format(t1_stop-t1_start))
        print("CPU process time: {:.1f} seconds".format(t2_stop-t2_start))
        print()

        # align reads to graph
        t1_start = time.perf_counter()
        t2_start = time.process_time()
        print("Mapping reads to graph")
        if args.reverse:
            subprocess.check_call( vg +
                " map -f {0} -f {1} -x {2}.norm.xg -g {2}.norm.gcsa -k 16 -t {4} > {3}.gam".format(
                    args.forward, args.reverse, graph_name, aln_name, args.threads),
                shell=True
            )
        else:
            subprocess.check_call( vg +
                " map -f {0} -x {2}.norm.xg -g {2}.norm.gcsa -k 16 -t {4} > {3}.gam".format(
                    args.forward, args.reverse, graph_name, aln_name, args.threads),
                shell=True
            )

        # filter for primary alignments
        print("Filtering alignments...")
        subprocess.check_call( vg +
            " filter -r 0.90 -s 2 -fu -t {1} {0}.gam > {0}.filtered.gam".format(
            aln_name, args.threads),
            shell=True)
        t1_stop = time.perf_counter()
        t2_stop = time.process_time()
        print("Alignments ready")
        print("Elapsed time: {:.1f} seconds".format(t1_stop-t1_start))
        print("CPU process time: {:.1f} seconds".format(t2_stop-t2_start))
        print()

        # output json
        print("Converting gam to json format")
        subprocess.check_call( vg +
            " view -a {0}.filtered.gam > {1}".format(aln_name, aln_file),
            shell=True)

    # read graph and check for cycles
    contig_graph, paths = read_gfa(gfa_file)
    if is_DAG(contig_graph):
        print("Contig variation graph is acyclic :)")
    else:
        print("Contig variation graph contains cycles!!!")

    # compute node abundances corresponding to GFA file
    print("Computing node abundances...",)
    node_abundances, connection_counts = get_node_abundances(
            contig_graph, aln_file)

    # merge simple paths and compute average abundance counts
    print("Compressing graph...",)
    final_graph, final_paths = compress_graph2(
            contig_graph, paths, node_abundances)
    V = len(list(final_graph.vertices()))
    E = len(list(final_graph.edges()))
    print("Final graph size: {} nodes and {} edges".format(V, E))

    # reorder nodes in topological order
    if is_DAG(final_graph):
        print("Final graph is acyclic :)")
        top_ordering = topological_sort(final_graph)
        final_graph, final_paths = reorder_graph(
                final_graph, final_paths, top_ordering)

    # write final graph and node abundances to file
    write_gfa(final_graph, '{}.final.gfa'.format(graph_name), paths=final_paths)
    final_graph.save('{}.final.gt'.format(graph_name))
    abundance_file = open('node_abundance.txt', 'w')
    for node in final_graph.vertices():
        v = int(node)+1
        if final_graph.vp.seq[node] == "N":
            # make sure ambiguous bases get low abundance to allow correction
            ab = 0 # also gets ignored during objective evaluation
        else:
            ab = final_graph.vp.ab[node]
        abundance_file.write("{}:{}\n".format(v, ab))
    abundance_file.close()
    t1_stop = time.perf_counter()
    t2_stop = time.process_time()
    print("\nbuild_graph_msga completed")
    print("Elapsed time: {:.1f} seconds".format(t1_stop-global_t1_start))
    print("CPU process time: {:.1f} seconds".format(t2_stop-global_t2_start))
    print()
    return

######################

def has_simple_paths(g):
    bad_edges = []
    for e in g.edges():
        if e.source().out_degree() == 1 and e.target().in_degree() == 1:
            bad_edges.append([e.source(), e.target()])
    if bad_edges != []:
        print(bad_edges)
        return True
    return False

def reorder_graph(g, paths, vertex_ordering):
    new_g = Graph(directed=True)
    new_g.vp.seq = new_g.new_vertex_property('string')
    new_g.vp.contigs = new_g.new_vertex_property('vector<string>')
    new_g.vp.ab = new_g.new_vertex_property('float')
    old_to_new = {}
    for i, v in enumerate(vertex_ordering):
        old_to_new[v] = i
        node = new_g.add_vertex()
        new_g.vp.seq[node] = g.vp.seq[g.vertex(v)]
        new_g.vp.contigs[node] = g.vp.contigs[g.vertex(v)]
        new_g.vp.ab[node] = g.vp.ab[g.vertex(v)]
    for e in g.edges():
        v1 = old_to_new[int(e.source())]
        v2 = old_to_new[int(e.target())]
        new_g.add_edge(v1, v2)
    new_paths = {}
    for k, p in paths.items():
        new_p = []
        for v in p:
            new_p.append(old_to_new[v])
        new_paths[k] = new_p
    return new_g, new_paths

def compress_graph2(g, contig_paths, node_abundances):
    # find simple edges
    simple_edges = []
    adj_in = {}
    adj_out = {}
    for e in g.edges():
        if e.source().out_degree() == 1 and e.target().in_degree() == 1:
            assert e.source() != e.target()
            simple_edges.append([e.source(), e.target()])
            adj_out[int(e.source())] = e
            adj_in[int(e.target())] = e

    # build simple paths from simple edges
    def extend_path(p):
        v = int(p[-1])
        if v in adj_out:
            p.append(adj_out[v].target())
            return extend_path(p)
        else:
            return p
    simple_paths = []
    for v, e in adj_out.items():
        if v not in adj_in:
            p = extend_path([e.source(), e.target()])
            simple_paths.append(p)

    # merge simple paths and store new node abundances in graph
    del_list = []
    replacement_dict = {}
    g.vp.ab = g.new_vertex_property('float')
    for v in g.vertices():
        g.vp.ab[v] = node_abundances[int(v)+1]
    for path in simple_paths:
        # compute average weighted abundance of path
        total_len = 0
        total_cov = 0
        total_seq = ""
        contigs = []
        for v in path:
            seq = g.vp.seq[v]
            total_seq += seq
            total_len += len(seq)
            total_cov += len(seq) * g.vp.ab[v]
            contigs += g.vp.contigs[v]
        av_cov = total_cov / total_len
        # merge path into single node
        start_node = path[0]
        g.vp.seq[start_node] = total_seq
        g.vp.ab[start_node] = av_cov
        g.vp.contigs[start_node] = list(set(contigs))
        for v in path[-1].out_neighbors():
            g.add_edge(start_node, v)
        del_list += path[1:]
        for v in path[1:]:
            replacement_dict[v] = path[0]
    # keep track of vertex IDs that were kept, in order to map to new IDs
    keep_list = []
    for v in g.vertices():
        if v not in del_list:
            keep_list.append(int(v))
    # now remove nodes from simple paths
    g.remove_vertex(del_list)
    assert has_simple_paths(g) == False

    # update contig paths
    old2new_IDs = {x : i  for i, x in enumerate(keep_list)}
    updated_paths = {}
    for k, node_list in contig_paths.items():
        new_path = []
        for v in node_list:
            try:
                v_new = old2new_IDs[int(v)]
            except KeyError:
                if len(new_path) == 0:
                    v_new = old2new_IDs[int(replacement_dict[v])]
                else:
                    continue
            new_path.append(v_new)
        updated_paths[k] = new_path

    return g, updated_paths

def compress_graph(g, contig_paths, node_abundances):
    """
    Compress the variation graph: merge any non-branching paths into a single
    node and update all node properties accordingly. Returns the updated graph
    and correspondingly updated contig paths.
    """
    # add dummy source node
    source = g.add_vertex()
    for v in g.vertices():
        if v.in_degree() == 0:
            g.add_edge(source, v)
    # traverse graph through DFS
    dfs_edges = dfs_iterator(g, source)
    simple_paths = []
    current_path = []
    for e in dfs_edges:
        # check if dfs continues on current path
        if len(current_path) > 0 and e.source() != current_path[-1]:
            current_path = []
        v = e.target()
        if v.in_degree() == 1:
            current_path.append(v)
            if v.out_degree() == 1:
                # simple path continues
                continue
        # end of current simple path
        if len(current_path) > 1:
            if current_path[0] in current_path[-1].out_neighbors():
                # path forms a cycle -> exclude final node
                simple_paths.append(current_path[:-1])
            else:
                simple_paths.append(current_path)
            # print([int(x) for x in current_path])
        # start new path
        if v.out_degree() == 1:
            current_path = [v]
        else:
            current_path = []
    g.remove_vertex(source)
    print("Number of simple paths: {}".format(len(simple_paths)))

    # merge simple paths and store new node abundances in graph
    del_list = []
    g.vp.ab = g.new_vertex_property('float')
    for v in g.vertices():
        g.vp.ab[v] = node_abundances[int(v)+1]
    for path in simple_paths:
        # compute average weighted abundance of path
        total_len = 0
        total_cov = 0
        total_seq = ""
        contigs = []
        for v in path:
            seq = g.vp.seq[v]
            total_seq += seq
            total_len += len(seq)
            total_cov += len(seq) * g.vp.ab[v]
            contigs += g.vp.contigs[v]
        av_cov = total_cov / total_len
        # merge path into single node
        start_node = path[0]
        g.vp.seq[start_node] = total_seq
        g.vp.ab[start_node] = av_cov
        g.vp.contigs[start_node] = list(set(contigs))
        for v in path[-1].out_neighbors():
            g.add_edge(start_node, v)
        del_list += path[1:]
    # keep track of vertex IDs that were kept, in order to map to new IDs
    keep_list = []
    for v in g.vertices():
        if v not in del_list:
            keep_list.append(int(v))
    # now remove nodes from simple paths
    g.remove_vertex(del_list)
    assert has_simple_paths(g) == False

    # update contig paths
    old2new_IDs = {x : i  for i, x in enumerate(keep_list)}
    updated_paths = {}
    for k, node_list in contig_paths.items():
        new_path = []
        for v in node_list:
            try:
                v_new = old2new_IDs[int(v)]
            except KeyError:
                continue
            new_path.append(v_new)
        updated_paths[k] = new_path

    return g, updated_paths


def write_gfa(g, gfa_file, paths=None):
    """
    Write graph-tool graph to a gfa format storing nodes, edges, and paths.
    Returns nothing.
    """
    contigs = {} # dict mapping contigs to lists of nodes
    with open(gfa_file, 'w') as file:
        file.write("H\tVN:Z:1.0") # header line GFA 1.0
        for v in g.vertices():
            file.write("\nS\t{}\t{}".format(int(v)+1, g.vp.seq[v]))
            for w in v.out_neighbors():
                file.write("\nL\t{}\t+\t{}\t+\t0M".format(int(v)+1, int(w)+1))
            for k in g.vp.contigs[v]:
                if k in contigs.keys():
                    contigs[k].append(v)
                else:
                    contigs[k] = [v]

        if paths != None:
            if len(paths) > 0:
                # write paths from path list
                path_info = paths
            else:
                # write paths based on graph traversal
                path_info = contigs
            for k, node_list in path_info.items():
                if len(node_list) == 0:
                    print("skipping length 0 path")
                    continue
                path = ''
                match = ''
                # for v in contigs[k]:
                for v in node_list:
                    path += str(int(v)+1)+'+,'
                    match += str(len(g.vp.seq[v]))+'M,'
                path = path.rstrip(',')
                match = match.rstrip(',')
                file.write("\nP\tp{}\t{}\t{}".format(k, path, match))
    return

def read_gfa(gfa_file):
    """
    Reads a graph from a GFA-file and returns graph in gt-format.
    """
    # Define a graph with its vertex properties
    g = Graph(directed=True)
    g.vp.seq = g.new_vertex_property('string')
    g.vp.contigs = g.new_vertex_property('vector<string>')

    # read gfa and add vertices to graph
    with open(gfa_file, 'r') as f:
        for line in f:
            line = line.rstrip('\n').split('\t')
            if line[0] == 'S':
                # vertex
                node_id = int(line[1]) - 1
                v = g.add_vertex()
                seq = line[2].upper()
                if len(seq) == 0:
                    print(line)
                g.vp.seq[v] = seq

    # parse through gfa again to add edges and contig paths to graph
    paths = {}
    path_count = 0
    with open(gfa_file, 'r') as f:
        for line in f:
            line = line.rstrip('\n').split('\t')
            if line[0] == 'L':
                # assert line[2] == line[4] == "+"
                v1 = int(line[1]) - 1
                v2 = int(line[3]) - 1
                g.add_edge(v1, v2)
            elif line[0] == 'P':
                path_count += 1
                contig_id = line[1]
                path = line[2].split(',')
                node_list = []
                path_ori = ""
                for node_info in path:
                    # take care of node orientations
                    ori = node_info[-1]
                    if path_ori != "":
                        if ori != path_ori:
                            # print("\nERROR: mixed paths. Please specify a larger bandwidth for alignment using the -w parameter.\n")
                            # sys.exit(1)
                            print("WARNING: mixed path for contig {}, skipping.".format(contig_id))
                            path_ori = "undefined"
                            break
                        assert ori == path_ori
                    else:
                        path_ori = ori
                    node = int(node_info.rstrip(ori)) - 1
                    node_list.append(node)
                    # store contig in node
                    g.vp.contigs[node].append(contig_id)
                if path_ori == "+":
                    paths[contig_id] = node_list
                elif path_ori == "-":
                    paths[contig_id] = node_list[::-1]
                elif path_ori == "undefined":
                    continue
                else:
                    print("ERROR: orientation unknown. Exiting.")
                    sys.exit(1)

    # # sanity check: make sure that every node is supported by at least one contig
    # del_list = []
    # for v in g.vertices():
    #     if len(g.vp.contigs[v]) == 0:
    #         del_list.append(v)
    #         print("No contigs for vertex {}, deleting.".format(int(v)))
    # g.remove_vertex(del_list)

    print("Vertex count: {}".format(len(list(g.vertices()))))
    print("Edge count: {}".format(len(list(g.edges()))))
    print("Path count: {}".format(path_count))

    # # save graph in gt format
    # g.save(graph_name + '.gt')
    return g, paths


def get_node_abundances(graph, aln_file):
    """
    Reads vg alignments and computes node abundance values and edge abundances
    (connection counts per edge).
    Returns node abundance list and connection count dict.
    """
    nodes = {}
    for node in graph.vertices():
        # update node ID, because alignments are to GFA graph which has 1-based
        # node IDs
        node_id = str(int(node)+1)
        seq = graph.vp.seq[node]
        nodes[node_id] = seq

    bases_per_node = {} # map node IDs to read alignments
    for node in nodes:
        bases_per_node[node] = 0

    # count node connections used by read alignments
    connection_counts = {}

    print("Processing alignments...")
    with open(aln_file, 'r') as aln_json:
        # for line in tqdm(aln_json):
        for line in aln_json:
            aln = json.loads(line)
            seq_id = aln["name"]
            seq = aln["sequence"]
            try:
                path = aln["path"]
                mapping = path["mapping"]
            except KeyError:
                # read unmapped
                continue
            offset = 0
            seq_aln_len = 0
            node0 = ""
            node1 = ""
            node2 = ""
            for node_info in mapping:
                position = node_info["position"]
                node_id = position["node_id"]
                node2 = int(node_id)-1
                node_seq = nodes[node_id]
                node_len = len(node_seq)
                aln_len = 0
                try:
                    offset = int(position["offset"])
                except KeyError:
                    offset = 0
                edit = node_info["edit"]
                for aln_piece in edit:
                    try:
                        from_len = int(aln_piece["from_length"])
                    except KeyError:
                        from_len = 0
                    try:
                        to_len = int(aln_piece["to_length"])
                    except KeyError:
                        to_len = 0
                    aln_len += min(from_len, to_len)
                # bases_per_node[node_id] += node_len - offset
                bases_per_node[node_id] += aln_len
                #node_to_seq[node_id].append(seq_id)
                seq_aln_len += aln_len
                if node0 != "":
                    # store 2-edge connection
                    if (node0, node2) in connection_counts:
                        try:
                            connection_counts[(node0, node2)][node1] += 1
                        except KeyError:
                            connection_counts[(node0, node2)][node1] = 1
                    else:
                        connection_counts[(node0, node2)] = {node1 : 1}
                if node1 != "":
                    # store direct connection
                    if (node1, node2) in connection_counts:
                        try:
                            connection_counts[(node1, node2)][-1] += 1
                        except KeyError:
                            connection_counts[(node1, node2)][-1] = 1
                    else:
                        connection_counts[(node1, node2)] = {-1 : 1}
                node0 = node1
                node1 = node2

    # TODO: check for double alignments and weight them accordingly
    # for node, seq_list in node_to_seq.items():
    #     for seq in seq_list:
    #         if seq_list.count(seq) > 2:
    #     #if len(set(seq_list)) != len(seq_list):
    #             print("DUPLICATE READ {} IN SEQ LIST OF NODE {}".format(seq, node))
    #             print(seq_list)
    #             sys.exit(1)

    # node_abundance_file = "node_abundance.txt"
    node_abundance_list = {}
    print("Computing node abundance rates...")
    # with open(node_abundance_file, 'w') as f:
    for node, seq in nodes.items():
        node_len = len(seq)
        aligned_len = bases_per_node[node]
        if node_len > 0:
            node_abundance = aligned_len / node_len
        else:
            print("Node length 0 for node {} ?!".format(node))
            node_abundance = 0
        node_abundance_list[int(node)] = node_abundance
        # print(node, ":", node_abundance)
        # f.write("{0}:{1}\n".format(node-1, node_abundance))

    return node_abundance_list, connection_counts


def cyclic(graph):
    """Return True if the directed input graph has a cycle."""
    visited = set()
    path = [object()]
    path_set = set(path)
    stack = [graph.vertices()]
    while stack:
        for v in stack[-1]:
            if v in path_set:
                print(list(path_set))
                print(v)
                return True
            elif v not in visited:
                visited.add(v)
                path.append(v)
                path_set.add(v)
                stack.append(iter(v.out_neighbors()))
                break
        else:
            path_set.remove(path.pop())
            stack.pop()
    return False


if __name__ == '__main__':
    sys.exit(main())
